//GUIA MANEJO STRINGS EJERCICIO 1
//Permitir que el usuario ingrese una palabra de hasta 20 letras.
//Mostrar en pantalla cuántas letras tiene la palabra ingresada.
//Por ejemplo "CASA" debe indicar 4 letras.

#include <stdio.h>

int main()
{
    char letras[20]; //Declaro las variables.
    int j = 0;

    printf("Ingrese una palabra: "); //Solicito el ingreso de una palabra.
    scanf("%s", letras);

    for(int i = 0; i <= 19 && letras[i]!='\0'; i++) //Recorro el string sumandole 1 a la variable por cada letra de la palabra.
    
    {
        j++;
    }

    printf("La palabra ingresada tiene %d letras", j); //Muestro cuántas letras tiene la palabra ingresada.
    
    return 0;
}